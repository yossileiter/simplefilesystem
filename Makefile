GCC=/usr/bin/gcc

fs: cli.o fs.o disk.o
	$(GCC) cli.o fs.o disk.o -o fs

cli.o: cli.c cli.h
	$(GCC) -Wall cli.c -c -o cli.o -g

fs.o: fs.c fs.h
	$(GCC) -Wall fs.c -c -o fs.o -g

disk.o: disk.c disk.h
	$(GCC) -Wall disk.c -c -o disk.o -g

clean:
	rm fs disk.o fs.o cli.o
#include "fs.h"
#include "disk.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define FS_MAGIC           0xf0f03410
#define INODES_PER_BLOCK   128
#define POINTERS_PER_INODE 5
#define POINTERS_PER_BLOCK 1024

struct fs_superblock {
	int magic;
	int nblocks;
	int ninodeblocks;
	int ninodes;
};

struct fs_inode {
	int isvalid;
	int size;
	int direct[POINTERS_PER_INODE];
	int indirect;
};

union fs_block {
	struct fs_superblock super;
	struct fs_inode inode[INODES_PER_BLOCK];
	int pointers[POINTERS_PER_BLOCK];
	char data[DISK_BLOCK_SIZE];
};

int number_of_data_blocks()
{
    int nblocks = disk_size();

    union fs_block block;
    disk_read(0, block.data);
    int ninodeblocks = block.super.ninodeblocks;
    
    return nblocks-1-ninodeblocks;
}

char free_data_block_bitmap[1000];

int fs_format()
{
	union fs_block block;
    
    block.super.magic = FS_MAGIC;
    block.super.nblocks = disk_size();
    block.super.ninodeblocks = block.super.nblocks/10 + ((block.super.nblocks%10 == 0) ? 0 : 1);
    block.super.ninodes = block.super.ninodeblocks*INODES_PER_BLOCK;

    disk_write (0,block.data);
    
    int ninodeblocks = block.super.ninodeblocks;
    for (size_t inode = 0; inode < INODES_PER_BLOCK; inode++)
    {
        block.inode[inode].isvalid = 0;
    }

    for (size_t blockinode = 1; blockinode <= ninodeblocks; blockinode++)
    {
        disk_write (blockinode, block.data);
    }
    return 1;
}

int fs_mount()
{
    union fs_block super;
    disk_read(0, super.data);
    int ninodeblocks = super.super.ninodeblocks; 
    
    for (size_t h = 0; h < 1 + ninodeblocks; h++)
    {
        free_data_block_bitmap[h] = 1;
    }

	for (size_t i = 1; i <= super.super.ninodeblocks; i++) 
    {
        
        union fs_block block;
        disk_read(i, block.data);
        for (size_t j = 0; j < INODES_PER_BLOCK; j++)
        {
            
            if (block.inode[j].isvalid == 1)
            {
                if (block.inode[j].size > 0)
                {
                    for (size_t k = 0; k < POINTERS_PER_INODE; k++)
                    {
                        
                        if (block.inode[j].direct[k] == 0) break;
                        free_data_block_bitmap[block.inode[j].direct[k]] = 1;
                                      
                    }
                    if (block.inode[j].indirect != 0) 
                    {
                        free_data_block_bitmap[block.inode[j].indirect] = 1;
                        
                        union fs_block pointers_block;
                        disk_read(block.inode[j].indirect, pointers_block.data);
                        for (size_t l = 0; l < POINTERS_PER_BLOCK; l++)
                        {
                            //printf("%ld pointers: %d\n",l, pointers_block.pointers[l]);
                            if (pointers_block.pointers[l] != 0)
                            {
                                free_data_block_bitmap[pointers_block.pointers[l]] = 1;
                            }
                            else break;
                        }
                    }  else continue;                
                } else continue;
            } else continue;
        }
    
    }

    return 1;
}

int fs_debug()
{
    union fs_block block;

	disk_read(0,block.data);

    if (block.super.magic == FS_MAGIC) 
    {
        printf("magic number is valid\n");
        printf("--------------------\n");
        printf("superblock:\n");
        printf("    %d blocks\n",block.super.nblocks);
        printf("    %d inode blocks\n",block.super.ninodeblocks);
        printf("    %d inodes\n",block.super.ninodes);
        
        for (size_t inode_block = 1; inode_block <= block.super.ninodeblocks; inode_block++)
        {
            disk_read(inode_block, block.data);

            for (size_t inode = 0; inode < INODES_PER_BLOCK; inode++)
            {
                if (block.inode[inode].isvalid == 1)
                {
                    printf("--------------------\n");
                    printf("inode %ld:\n", inode+1+((inode_block-1)*INODES_PER_BLOCK));
                    printf("    size: %d bytes\n",block.inode[inode].size);
                    if (block.inode[inode].size > 0) 
                    {   
                        printf("    direct blocks: ");
                        for (size_t k = 0; k < POINTERS_PER_INODE; k++)
                        {
                            if (block.inode[inode].direct[k] == 0) break; 
                            printf("%d ",block.inode[inode].direct[k]);
                        }

                        if (block.inode[inode].indirect != 0)
                        {
                            printf("\n    indirect block: %d\n",block.inode[inode].indirect);
                            printf("    ↘️ indirect data blocks: ");
                            
                            union fs_block pointers_block;
                            disk_read(block.inode[inode].indirect, pointers_block.data);
                            
                            for (size_t l = 0; l < POINTERS_PER_BLOCK; l++)
                            {                               
                                if (pointers_block.pointers[l] == 0) break;
                                printf("%d ", pointers_block.pointers[l]);
                            }
                            
                        } 
                        printf("\n");
                    } else continue;
                    
                } else continue;
                
            }
                       
        }
            
    }
    else printf("magic number is not valid. use <format>\n");

	return 1;

}

int fs_getsize(int inumber)
{
    union fs_block block;
    int nblock = divide_up(inumber);
    disk_read(nblock, block.data);

    int inode_index = get_inode_index(inumber);
    if (block.inode[inode_index].isvalid == 0) return -1;
	else return block.inode[inode_index].size;
}

int fs_create()
{
    union fs_block block;
    disk_read(0,block.data);
    int ninodeblocks = block.super.ninodeblocks;
    
    for (size_t blockinode = 1; blockinode <= ninodeblocks; blockinode++)
    {   
        disk_read(blockinode, block.data);
        for (size_t inode = 0; inode < INODES_PER_BLOCK; inode++)
        {
            if (block.inode[inode].isvalid == 0)
            {
                block.inode[inode].isvalid = 1;
                block.inode[inode].size = 0;
                block.inode[inode].indirect = 0;
                disk_write (blockinode, block.data);
                return inode+(INODES_PER_BLOCK*(blockinode-1))+1;              
            }                        
            else continue;
        }              
    } 
       
    return -1;
}

int fs_delete(int inumber)
{
	union fs_block block;
    int nblock = divide_up(inumber);
    disk_read(nblock, block.data);

    int inode_index = get_inode_index(inumber);
    if (block.inode[inode_index].isvalid == 0) printf("file not exist\n");
    else 
    {
        block.inode[inode_index].isvalid = 0;
        disk_write(nblock, block.data);
        for (size_t i = 0; i < POINTERS_PER_INODE; i++)
        {
           if (block.inode[inode_index].direct[i] == 0) break;
           free_data_block_bitmap[block.inode[inode_index].direct[i]] = 0;
        }
        if (block.inode[inode_index].indirect != 0)
        {
            union fs_block pointers_block;
            disk_read(block.inode[inode_index].indirect, pointers_block.data);
            for (size_t i = 0; i < POINTERS_PER_BLOCK; i++)
            {
                if (pointers_block.pointers[i] == 0) break;
                free_data_block_bitmap[pointers_block.pointers[i]] = 0;                
            }
            
            free_data_block_bitmap[block.inode[inode_index].indirect] = 0;
        }
    }
    return 1;
}

int fs_read(int inumber, char *data, int length, int offset)
{ 
    int nblock = divide_up(inumber);
    int inode_index = get_inode_index(inumber);
    int first_block_to_copy = offset/DISK_BLOCK_SIZE;
    int blocks_to_read = length/DISK_BLOCK_SIZE;
    
    int copied = 0;

    union fs_block data_block;
    union fs_block block;
    disk_read(nblock, block.data);

    if (block.inode[inode_index].isvalid == 0) return -1; //should return file not exist
    if (offset > block.inode[inode_index].size) return 0;
    
    for (size_t i = first_block_to_copy; i < blocks_to_read+first_block_to_copy; i++)
    {  
        int n_indirect_block = block.inode[inode_index].indirect;
        
        if (i < POINTERS_PER_INODE)
        {
            if (block.inode[inode_index].direct[i] == 0) break;
            int block_to_read = block.inode[inode_index].direct[i];
            
            disk_read(block_to_read, data_block.data);
            memcpy(data+copied, data_block.data, sizeof(data_block.data));
            
            copied += sizeof(data_block.data);
        }
        
        else if (n_indirect_block != 0)
        {
            union fs_block indirect_block;
            
            disk_read(n_indirect_block, indirect_block.data);
            disk_read(indirect_block.pointers[i-POINTERS_PER_INODE], indirect_block.data);
            memcpy(data+copied, data_block.data, sizeof(data_block.data));

            copied += sizeof(data_block.data);
        }
        else break;

    }
     

    return copied; 
}

int fs_write(int inumber, const char *data, int length, int offset)
{
	union fs_block super;
    disk_read(0, super.data);
    int ninodeblocks = super.super.ninodeblocks;    //number of inode blocks
    
    int blocks_to_read = length/DISK_BLOCK_SIZE + ((length%DISK_BLOCK_SIZE == 0) ? 0 : 1);    //number of data blocks we need to write
    int num_block_to_copy = offset/DISK_BLOCK_SIZE; //number of current data block to write

    printf("number of data blocks we need to write: %d\n", blocks_to_read);
    printf("number of current data block to write: %d\n", num_block_to_copy);

    int copied=0;                                   //copied bytes counter
    int nblock = divide_up(inumber);                //index of the inode block
    int inode_index = get_inode_index(inumber);     //index of the specific inode
    
    union fs_block block;
    disk_read(nblock, block.data);                  //
    
    if (offset > block.inode[inode_index].size) return 0;    

    else
    {
        block.inode[inode_index].isvalid = 1;
        disk_write(nblock, block.data);
        
        for (size_t m = num_block_to_copy; m < blocks_to_read+num_block_to_copy; m++)
        {
            for (size_t h = 1 + ninodeblocks; h < sizeof(free_data_block_bitmap); h++)
            {
                if (free_data_block_bitmap[h] == 1) continue;
                else           
                {
                    printf("free data block: %ld\n", h);
                    
                    union fs_block data_block;
                    memcpy(data_block.data, data+copied, length);
                                          
                        if (length-copied <= 0)
                        {
                            block.inode[inode_index].direct[m+1] = 0;
                            disk_write(nblock, block.data);
                            break;
                        }
                            
                        else block.inode[inode_index].direct[m] = h;
                        block.inode[inode_index].size = copied+length;
                        disk_write(nblock, block.data);
                    
                                    
                    printf("%d bytes to copy\n", copied+length);
                    disk_write(h, data_block.data);
                    
                    free_data_block_bitmap[h] = 1;
                    copied += length;
                    return length;
                

                                
                }
                
            }
        }
        

        block.inode[inode_index].isvalid = 1;
        disk_write(nblock, block.data);

    }
    return length;
}

int divide_up(int inumber)
{
    return inumber/(INODES_PER_BLOCK) + ((inumber%(INODES_PER_BLOCK) == 0) ? 0 : 1);    
}

int get_inode_index(int inumber)
{
    int nblock = divide_up(inumber);
    int inode_index = (inumber-INODES_PER_BLOCK*(nblock-1)-1);
	return inode_index;
}

int bitmap()
{
    union fs_block super;
    disk_read(0, super.data);
    int ninodeblocks = super.super.ninodeblocks; 
    
    printf("bitmap:  \n");  
    printf("------------\n");       
    printf("super block:\n");
    printf("0: %d\n", free_data_block_bitmap[0]);
    
    printf("------------\n");
    printf("inode blocks: %d\n", ninodeblocks);
    for (size_t i = 1; i < 1 + ninodeblocks; i++) 
    {
        printf("%ld: %d\n", i, free_data_block_bitmap[i]);
    }
    
    printf("------------\n");
    printf("data blocks:\n");
    for (size_t i = 1 + ninodeblocks; i < 100; i++) 
    {
        printf("%ld: %d\n", i, free_data_block_bitmap[i]);
    }
    
    return 1;
}

int validation_check(int inumber)
{
    int inode_index = get_inode_index(inumber);     //index of the specific inode
    int nblock = divide_up(inumber);                //index of the inode block
    union fs_block block;
    disk_read(nblock, block.data); 

    if (block.inode[inode_index].isvalid == 1) return-1;    //check if this inode already contains a file    
    else return 1;
}

//כשמעתיקים קובץ גדול מ4KB הוא לא שומר טוב את הגודל וגם מוסיף בייטים בסוף
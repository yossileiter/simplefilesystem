#ifndef FS_H
#define FS_H

int fs_format();
int fs_mount();
int fs_debug();

int fs_getsize(int inumber);
int fs_create();
int fs_delete(int inumber);

int fs_write(int inumber, const char *data, int length, int offset);
int fs_read(int inumber, char *data, int length, int offset);

int divide_up(int inumber);
int get_inode_index(int inumber);
int number_of_data_blocks();
int bitmap();
int validation_check(int inumber)
#endif
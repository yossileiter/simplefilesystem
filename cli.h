static int do_copyin( const char *filename, int inumber );
static int do_copyout( int inumber, const char *filename );

int debug(int args);
int format(int args);
int mount(int args);
int cat(int args, char *arg1);
int copyout(int args, char *arg1, char *arg2);
int create(int args);
int delete(int args, char *arg1);
int copyin(int args, char *arg1, char *arg2);
void help();
int getsize(int args, char *arg1);
